﻿/*
  CONSULTA DE SELECCIÓN Y DE TOTALES
*/
-- 1	Listar las edades de todos los ciclistas de Banesto


SELECT
DISTINCT
  c.edad
FROM 
  ciclista c
WHERE 
  c.nomequipo = 'Banesto';

-- 2	Listar las edades de los ciclistas que son de Banesto o de Navigare

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto' 
    OR 
    c.nomequipo='Navigare';

  -- utilizar la clausula IN
  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo IN ('Banesto','Navigare');



-- 3	Listar el dorsal de los ciclistas que son de Banesto y cuya edad esta entre 25 y 32

  SELECT 
    dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;


-- 4	Listar el dorsal de los ciclistas que son de Banesto o cuya edad esta entre 25 y 32

  SELECT 
    dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;

-- 5	Listar la inicial del equipo de los ciclistas cuyo nombre comience por R

  SELECT 
    DISTINCT LEFT(c.nomequipo,1) 
  FROM 
    ciclista c 
  WHERE 
    c.nombre LIKE 'R%';

  -- sustituir el like por una funcion
  
  SELECT 
    DISTINCT LEFT(c.nomequipo,1) 
  FROM 
    ciclista c 
  WHERE 
    LEFT(c.nombre,1)='R';

  -- aunque no den problemas las mayusculas vamos a evitarlos

  SELECT 
    DISTINCT LEFT(c.nomequipo,1) 
  FROM 
    ciclista c 
  WHERE 
    UPPER(LEFT(c.nombre,1))='R';
  

  
-- 6	Listar el código de las etapas que su salida y llegada sea en la misma población.
  
  SELECT 
    e.numetapa 
  FROM 
    etapa e 
  WHERE 
    e.salida=e.llegada;

-- 7	Listar el código de las etapas que su salida y llegada no sean en la misma población y que conozcamos el dorsal del ciclista que ha ganado la etapa

  SELECT 
    numetapa 
  FROM 
    etapa e 
  WHERE 
    e.salida<>e.llegada AND e.dorsal IS NOT NULL;


-- 8	Listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400

  SELECT 
    p.nompuerto 
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1000 AND 2000 OR p.altura > 2400;


-- 9	Listar el dorsal de los ciclistas que hayan ganado los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400.
  
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1000 AND 2000 OR p.altura > 2400;

-- 10	Listar el número de ciclistas que hayan ganado alguna etapa
  
  SELECT COUNT(DISTINCT dorsal) AS nCiclistas FROM etapa e;

  -- esto en access hubiera sido
  -- c1 : ciclistas que han ganado etapas (sin repetidos)
  SELECT DISTINCT e.dorsal  FROM etapa e;
  -- 
  SELECT 
    COUNT(*) AS nCiclistas 
  FROM 
    (SELECT DISTINCT e.dorsal  FROM etapa e) AS c1;
      

-- 11	Listar el número de etapas que tengan puerto
  
  SELECT 
    COUNT(DISTINCT p.numetapa) AS nEtapas
  FROM 
    puerto p;


-- 12	Listar el número de ciclistas que hayan ganado algún puerto
  
  SELECT 
    COUNT(DISTINCT p.dorsal) AS nCiclistas 
  FROM 
    puerto p;


-- 13	Listar el código de la etapa con el número de puertos que tiene
  SELECT 
    numetapa,COUNT(*) AS nPuertos 
  FROM 
    puerto 
  GROUP BY 
    numetapa;

-- 14	Indicar la altura media de los puertos
  
  SELECT 
    AVG(p.altura) 
  FROM 
    puerto p;


-- 15	Indicar el código de etapa cuya altura media de sus puertos está por encima de 1500.
    
    SELECT 
     numetapa 
    FROM 
      puerto 
    GROUP BY 
      numetapa 
    HAVING 
      AVG(altura)>1500;

    -- no quiero utilizar el HAVING
    -- c1
    SELECT 
     numetapa,
     AVG(altura) AS aMedia
    FROM 
      puerto 
    GROUP BY 
      numetapa; 
    --
      SELECT 
        numetapa
      FROM 
        (SELECT numetapa, AVG(altura) AS aMedia FROM puerto GROUP BY numetapa) as c1
       WHERE 
        c1.aMedia>1500;


-- 16	Indicar el número de etapas que cumplen la condición anterior

-- c1
    SELECT 
     numetapa 
    FROM 
      puerto 
    GROUP BY 
      numetapa 
    HAVING 
      AVG(altura)>1500;
-- 
  SELECT 
    COUNT(*) AS nEtapas
  FROM (
    SELECT 
     numetapa 
    FROM 
      puerto 
    GROUP BY 
      numetapa 
    HAVING 
      AVG(altura)>1500
    ) AS C1;
  

-- 17	Listar el dorsal del ciclista con el número de veces que ha llevado algún maillot.
    SELECT 
      dorsal,COUNT(*) AS nMaillots 
    FROM 
      lleva 
    GROUP BY 
      dorsal;

-- 18	Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha llevado ese maillot.
  
  SELECT 
    l.dorsal, l.código, COUNT(*) AS numero 
  FROM 
    lleva l 
  GROUP BY 
    l.dorsal, l.código;

-- 19	Listar el dorsal el código de etapa, el ciclista y el numero de maillots que ese ciclista a llevado en cada etapa
  
  SELECT 
    dorsal, numetapa, COUNT(*) AS numero 
  FROM 
    lleva 
  GROUP BY dorsal, numetapa;

  -- si queremos sacar el nombre
    SELECT 
      c1.*, c.nombre
    FROM (
      SELECT 
        dorsal, numetapa, COUNT(*) AS numero 
      FROM 
        lleva 
      GROUP BY 
        dorsal, numetapa) AS c1 
    INNER JOIN 
      ciclista c 
    ON c1.dorsal=c.dorsal;

    -- hacer el join antes del group
    SELECT 
      c.dorsal,c.nombre,l.numetapa, COUNT(*) AS numero
    FROM 
      lleva l 
    JOIN 
      ciclista c 
      ON l.dorsal = c.dorsal
    GROUP BY
      c.dorsal,l.numetapa;
